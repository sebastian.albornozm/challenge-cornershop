# Project documentation

## Install
You'll need to create a python virtual enviroment, i suggest 'virtualenv' for simplicity and activate it.
Also you need to install the requerired packages with pip
```
virtualenv -p {your favorite python interpreter(python3.8)} {env_name(cornershop_env)}
source cornershop_env/bin/activate
```
```
pip install -r requirements.txt
```

## Database
This proyect is configured to run on a MySQL database, so you'll need to create one. Then, you have the enter some credencials into MySQL config file located under
(in my case) e.g '/etc/mysql/my.cnf'
```
[client]
database=nombre_bdd
user = Mysql_user
pass = Mysql_pass
default-character-set=utf8
```

With the database created and configured is necesary to create the migrations with
`python manage.py makemigrations`
 
and then migrate 
`python manage.py migrate`
(All of this has to be on the same directory where manage.py is located)

If u already have redis-server on your workspace please init.
`redis-server`

## Run the project

` python manage.py runserver`

Run celery broker together with the server
` celery -A cornershop beat -l info`

## Users
To load the Nora user use
```
python manage.py loaddata data/nora.json
```
Employees will write their rut while selecting a meal from the daily menu, to this case, if the employee isnt registrated it will be registered on the
employee table.

## Tests
Unit test where performed on the same file @ the main menu app.
To run the tests use
` python manage.py test`

To check the coverage on a html file use
`coverage html`

## Style guide
PEP8


# Challenge instructions 

# Cornershop's Backend Test 

This technical test requires the design and implementation (using Django) of a management system to coordinate the meal delivery for Cornershop employees.

## After you finish your test please share this repository with: @Cornershop-hr @Alecornershop @roherrera @stichy23

Should you have any technical questions, please contact osvaldo@cornershopapp.com
Tittle of the project: Backend-Test-(Last Name)

## Description

The current process consist of a person (Nora) sending a text message via Whatsapp to all the chilean employees, the message contains today's menu with the different alternatives for lunch. 

> Hello!  
> I share with you today's menu :)
>
> Option 1: Corn pie, Salad and Dessert  
> Option 2: Chicken Nugget Rice, Salad and Dessert  
> Option 3: Rice with hamburger, Salad and Dessert  
> Option 4: Premium chicken Salad and Dessert.
>
> Have a nice day!

With the new system, Nora should be able to:

- Create a menu for a specific date.
- Send a Slack reminder with today's menu to all chilean employees (this process needs to be asynchronous).

The employees should be able to:

- Choose their preferred meal (until 11 AM CLT).
- Specify customizations (e.g. no tomatoes in the salad).

Nora should be the only user to be able to see what the Cornershop employees have requested, and to create and edit today's menu. The employees should be able to specify what they want for lunch but they shouldn't be able to see what others have requested. 

NOTE: The slack reminders must contain an URL to today's menu with the following pattern https://nora.cornershop.io/menu/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx (an UUID), this page must not require authentication of any kind.

## Aspects to be evaluated

Since the system is very simple (yet powerful in terms of yumminess) we'll be evaluating, these aspects:

- Functionality
- Testing
- Documentation
- Software design
- Programming style
- Appropriate framework use

## Aspects to be ignored

- Visual design of the solution
- Deployment of the solution

## Restrictions

- The usage of Django's admin is forbidden.

## Developed by Sebastian Albornoz Medina