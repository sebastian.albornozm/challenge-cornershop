from django import forms
from .models import Menu, Meal, Order


class CreateMealForm(forms.ModelForm):
    class Meta:
        model = Meal
        fields = ['menu', 'name']


class CreateMenuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields =['scheduled_to']


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['meal', 'customization']

    def __init__(self, **kwargs):
        super(OrderForm, self).__init__(**kwargs)
        menu = kwargs.get("initial").get("menu")
        self.fields['meal'].queryset = Meal.objects.filter(menu=menu)


class EmployeeOrderForm(OrderForm):
    rut = forms.CharField(max_length=10)
    class Meta(OrderForm.Meta):
        fields = OrderForm.Meta.fields + ['rut']
