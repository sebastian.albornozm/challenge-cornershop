from django.db import models
from django.utils.timezone import now
from django.core.exceptions import ValidationError

import uuid

from .utils import before_elevenAm

class Menu(models.Model):
    _id = models.AutoField(primary_key=True)
    _optionsNumber = 4
    created_at = models.DateField(default=now())
    scheduled_to = models.DateField()
    uuid = models.UUIDField(default=uuid.uuid4)


    class Meta:
        verbose_name = 'Menu'
        ordering = ['scheduled_to']

    def __str__(self):
        return f"number of options = {self._optionsNumber}, and was created at {self.created_at}, scheduled to: {self.scheduled_to}"


class Meal(models.Model):
    _id = models.AutoField(primary_key=True)
    created_at = models.DateField(default=now())
    name = models.CharField(max_length=100)
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)


    class Meta:
        verbose_name = 'Meal'
        ordering = ['created_at']

    def __str__(self):
        return f"{self.name}."


class Employee(models.Model):
    rut = models.CharField(max_length=10, null=True, blank=True)
    created_at = models.DateField(default=now())
    updated_at = models.DateTimeField(now(), null=True)


    class Meta:
        verbose_name = 'Employee'

    def __str__(self):
        return self.rut


class Order(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.DO_NOTHING)
    meal = models.ForeignKey(Meal, on_delete=models.DO_NOTHING)
    menu = models.ForeignKey(Menu, on_delete=models.DO_NOTHING)
    customization = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateField(default=now())
    updated_at = models.DateTimeField(null=True)


    class Meta:
        verbose_name = 'Employee order'

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if before_elevenAm is False:
            raise ValidationError("Today's menu has benn closed")
        super(Order, self).save(*args, **kwargs)
