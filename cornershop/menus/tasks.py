from celery.decorators import periodic_task
from celery.utils.log import get_task_logger

from django.conf import settings

import uuid

from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

from menus.models import Menu, Meal


# Generate tasks to be executed by celery.
@periodic_task(name='send_slack_message')
def send_slack_message(uuid):
    """
    Make and send the slack message containing the menu
     every work day @ 9.30AM.
    """
    client = WebClient(token=settings.SLACK_API_KEY)
    menu_url = settings.MENU_URL.format(uuid)
    today_meals = "I share today's menu :3 \n"
    if Menu.objects.filter(uuid=uuid) is not Menu.DoesNotExist:
        today_menu = Menu.objects.get(uuid=uuid)
        meals = Meal.objects.filter(menu=today_menu)
        for _ in meals:
            today_meals += str(_.name) + "\n"
    final_message = settings.SLACK_MESSAGE_TEXT.format(today_meals, menu_url)
    try:
        response = client.chat_postMessage(
            channel=settings.SLACK_CHANNEL,
            text=final_message
        )
    except SlackApiError as e:
        # You will get a SlackApiError if "ok" is False
        assert e.response["error"]
