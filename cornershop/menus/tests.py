from django.test import TestCase, Client
from django.utils.timezone import now
from django.urls import reverse

import uuid

from .models import Menu, Meal, Order, Employee

# form test imports
from http import HTTPStatus
from .forms import EmployeeOrderForm, CreateMenuForm, CreateMealForm


# Create your tests here.
class MenuCreateTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set object to test with
        """
        cls.object_id = Menu.objects.create(scheduled_to="2021-02-05", created_at=now()).pk

    def test_menu_created(self):
        """
        Check if created menu is an instance of the model
        """
        menu = Menu.objects.get(pk=self.object_id)
        self.assertTrue(isinstance(menu, Menu))

    def test_menu_updated(self):
        menu = Menu.objects.get(pk=self.object_id)
        menu.scheduled_to = "2021-02-04"
        menu.save()
        # Still a menu
        self.assertTrue(isinstance(menu, Menu))
        # get menu from db and compare to object
        menu_fromdb = Menu.objects.get(pk=self.object_id)
        self.assertEqual(menu, menu_fromdb)

    def test_menu_invalid_date(self):
        """
        Search for a menu in @ unregistered date.
        """
        past_date = "2019-09-18"
        with self.assertRaises(Menu.DoesNotExist):
            Menu.objects.get(scheduled_to=past_date)

    def test_valid_uuid(self):
        menu = Menu.objects.get(pk=self.object_id)
        self.assertTrue(menu.uuid, uuid.uuid4)
    

class EmployeeCreateTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set object to test with
        """
        cls.object_id = Employee.objects.create(rut="18731363-5", created_at=now(), updated_at=None).pk
    
    def test_employee_created(self):
        """
        Check if created employee is an instance of the model
        """
        employee = Employee.objects.get(pk=self.object_id)
        self.assertTrue(isinstance(employee, Employee))

    def test_employee_updated(self):
        employee = Employee.objects.get(pk=self.object_id)
        employee.rut = "12345678-9"
        employee.save()
        # Still a employee
        self.assertTrue(isinstance(employee, Employee))
        # Get employee from db and compare
        employee_fromdb = Employee.objects.get(pk=self.object_id)
        self.assertEqual(employee, employee_fromdb)
        

class MealCreateTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set object to test with
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk
        
    def test_meal_created(self):
        """
        Check if created meal is an instance of the model
        """
        menu = Menu.objects.get(pk=self.menu_id)
        Meal.objects.create(menu=menu, name="Meal_test", created_at=now())
        meal = Meal.objects.all()[0]
        self.assertTrue(isinstance(meal, Meal))

    def test_meal_updated(self):
        menu = Menu.objects.get(pk=self.menu_id)
        Meal.objects.create(menu=menu, name="Meal_test", created_at=now())
        meal = Meal.objects.all()[0]
        meal.name = "New meal test name"
        meal.save()
        meal_fromdb = Meal.objects.all()[0]
        self.assertEqual(meal, meal_fromdb)

class OrderCreateTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set relationship objects to test with
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk
        cls.employee_id = cls.object_id = Employee.objects.create(rut="18731363-5", created_at=now(), updated_at=None).pk

    def test_create_order(self):
        """
        Check if created order is an instance of the model
        """
        menu = Menu.objects.get(pk=self.menu_id)
        meal = Meal.objects.create(name="Aggregate onion rings", menu=menu, created_at=now())
        employee = Employee.objects.get(pk=self.employee_id)
        customization = "+ french fries"
        Order.objects.create(menu=menu, employee=employee, customization=customization, created_at=now(), meal=meal)
        order = Order.objects.all().first()
        self.assertTrue(isinstance(order, Order))

    def test_update_order(self):
        menu = Menu.objects.get(pk=self.menu_id)
        meal = Meal.objects.create(name="Aggregate onion rings", menu=menu, created_at=now())
        employee = Employee.objects.get(pk=self.employee_id)
        customization = "+ french fries"
        Order.objects.create(menu=menu, employee=employee, customization=customization, created_at=now(), meal=meal)
        order = Order.objects.all().first()
        order.customization = "Less french fries"
        order.save()
        order_fromdb = Order.objects.all().first()
        self.assertEqual(order, order_fromdb)


# forms test
class CreateMenuFormTest(TestCase):
    
    def test_correct_input(self):
        """
        If the form was called, check for redirect to the succes url.
        """
        response = self.client.post(
            "/menu/create/", data={ "scheduled_to":"2021-02-03"}
        )
        
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_form_valid(self):
        """
        Test if the form input is valid with incorrect date format.
        Format must be yyyy-mm-dd, provided format is dd-mm-yyyy.
        """
        scheduled_to = "12-12-2021"
        data = {"scheduled_to":scheduled_to}
        form = CreateMenuForm(data=data)

        self.assertFalse(form.is_valid())


class CreateMealFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set object to test with
        """
        cls.object_id = Menu.objects.create(scheduled_to="2021-02-05").pk

    def test_form_valid(self):
        """
        Test if the form input is valid.
        """
        menu = Menu.objects.get(pk=self.object_id)
        name = "test name for a meal"
        data = {
            "menu":menu,
            "name":name
        }
        form = CreateMealForm(data=data)
        self.assertTrue(form.is_valid())


class EmployeeOrderFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set relationship objects to test with
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk
        cls.employee_id = cls.object_id = Employee.objects.create(rut="18731363-5", created_at=now(), updated_at=None).pk

    def test_form_valid(self):
        """
        Test if form data is valid.
        """
        menu = Menu.objects.get(pk=self.menu_id)
        meal = Meal.objects.create(menu=menu, name="Salad")
        employee = Employee.objects.get(pk=self.employee_id)
        customization = "+ Onion rings"
        data = {
            "rut":employee,
            "customization":customization,
            "meal":meal,
            "menu":menu
        }
        form = EmployeeOrderForm(data=data)
        self.assertFalse(form.is_valid())


# views tests
class ListMenuViewTest(TestCase):

    def test_menu_list(self):
        """
        Test for request the list of menus.
        """
        url = reverse("list-menu")
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)


class CreateMenuViewTest(TestCase):

    def test_create_menu_view(self):
        """
        Test for request(get) the create menu view(who calls the form respectively).
        """
        url = reverse("create-menu")
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)


class UpdateMenuViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set object to test with
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk
    
    def test_update_menu_view(self):
        """
        Test for request(get) the update menu view(also calls the respective form).
        """
        url = reverse("menu-update",args=[self.menu_id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)


class CreateMealViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set object to test with.
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk

    def test_create_meal_view(self):
        """
        Test get method on create meal.
        """
        url = reverse("create-meal")
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
    

class UpdateMealViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set object to test with
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk

    def test_update_meal_view(self):
        """
        Test get method.
        """
        menu = Menu.objects.get(pk=self.menu_id)
        meal = Meal.objects.create(name="test-meal",menu=menu)
        meal_pk = meal._id
        url = reverse("update-meal", args=[meal_pk])
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)


class ListMealViewTest(TestCase):

    def test_menu_list(self):
        """
        Test for request the list of menus.
        """
        url = reverse("list-meals")
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)


class ListOrderViewTest(TestCase):

    def test_menu_list(self):
        """
        Test for request get list of meals.
        """
        url = reverse("list-order")
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.FOUND)


class UpdateOrderViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set relationship objects to test with.
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk
        cls.employee_id = Employee.objects.create(rut="18731363-5", created_at=now(), updated_at=None).pk

    def test_order_update(self):
        """
        Test for request get update order.
        """
        customization = "Plus french fries"
        menu = Menu.objects.get(pk=self.menu_id)
        meal = Meal.objects.create(name="Onion rings", menu=menu)
        employee = Employee.objects.get(pk=self.employee_id)

        order = Order.objects.create(customization=customization, menu=menu, meal=meal, employee=employee)
        url = reverse("update-order", args=[menu.uuid, order.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.OK)


class CreateOrderViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Set relationship objects to test with.
        """
        cls.menu_id = Menu.objects.create(scheduled_to="2021-02-06", created_at=now()).pk
        cls.employee_id = Employee.objects.create(rut="18731363-5", created_at=now(), updated_at=None).pk

    def test_order_create(self):
        """
        Test for request get create order.
        """
        customization = "Plus french fries"
        menu = Menu.objects.get(pk=self.menu_id)
        meal = Meal.objects.create(name="Onion rings", menu=menu)
        employee = Employee.objects.get(pk=self.employee_id)
        order = Order.objects.create(customization=customization, menu=menu, meal=meal, employee=employee)

        url = reverse("create-order", args=[menu.uuid])
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.OK)
