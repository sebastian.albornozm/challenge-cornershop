from django.test import TestCase
from django.utils.timezone import now

import uuid

from .models import Menu, Meal, Order, Employee

# Create your tests here.
class MenuCreateTest(TestCase):
    def setUp(self):
        Menu.objects.create(scheduled_to="2021-02-05", created_at=now())

    def test_menu_created(self):
        menu = Menu.objects.get(_id=1)
        self.assertTrue(isinstance(menu, Menu))

    def test_menu_updated(self):
        menu = Menu.objects.get(_id=1)
        menu.scheduled_to = "2021-02-04"
        menu.save()
        # Still a menu
        self.assertTrue(isinstance(menu, Menu))
        # get menu from db and compare to object
        menu_fromdb = Menu.object.get(_id=1)
        self.assertEqual(menu, menu_fromdb)



class EmployeeCreateTest(TestCase):
    def setUp(self):
        Employee.objects.create(rut="18731363-5", created_at=now(), updated_at=None)
    
    def test_employee_created(self):
        employee = Employee.objects.get(rut="18731363-5")
        self.assertTrue(isinstance(employee, Employee))
        
class MealCreateTest(TestCase):
    def setUp(self):
        Menu.objects.create(scheduled_to="2021-02-06", created_at=now())
        # menu = Menu.objects.get(scheduled_to="2021-02-06")
        Meal.objects.create(name="Meal_test", created_at=now())
        
    def test_meal_created(self):
        meal = Meal.objects.get(id=1)
        self.assertTrue(isinstance(meal, Meal))