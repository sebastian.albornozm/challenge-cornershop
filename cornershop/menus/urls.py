from django.urls import path
from django.views.generic import TemplateView

from .views import CreateMenu, UpdateMenu, ListMenu, CreateMeal, ListMeal, UpdateMeal, ListOrder, CreateOrder, UpdateOrder, todays_menu

urlpatterns = [
    # First page show list of menus created.
    path('', ListMenu.as_view(), name="first-page"),

    # Menu urls.
    path('list/', ListMenu.as_view(), name="list-menu"), # Only available for Nora.
    path('create/', CreateMenu.as_view(), name="create-menu"), # Only available for Nora.
    path('update/<int:pk>/', UpdateMenu.as_view(), name='menu-update'), # Only available for Nora.
    path('today/', todays_menu, name="todays-menu"),

    # Meals urls.
    path('meal/create', CreateMeal.as_view(), name='create-meal'), # Only available for Nora.
    path('meal/update/<int:pk>', UpdateMeal.as_view(), name='update-meal'), # Only available for Nora.
    path('meals/', ListMeal.as_view(), name='list-meals'), # Only available for Nora.

    # Order urls.
    path('orders/', ListOrder.as_view(), name='list-order'), # Only available for Nora.
    path('<uuid:uuid>/', CreateOrder.as_view(), name='create-order'), # Available for everyone.
    path('<uuid:uuid>/<int:pk>', UpdateOrder.as_view(), name='update-order'), # Available for everyone.
    path('orders/thanks/', TemplateView.as_view(template_name="orders/thanks.html")), # Available for everyone.

]
