from django.shortcuts import render, redirect
from django.utils.timezone import now

# Generate utils functions

def before_elevenAm(date):
    current_time = datetime.now()
    eleven_am = (datetime.now()).replace(hour=11, minute=0)
    return current_time < eleven_am

