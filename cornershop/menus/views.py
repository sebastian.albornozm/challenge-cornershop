from django.shortcuts import render
from django.views.generic import FormView, CreateView, UpdateView, ListView, DetailView
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.conf import settings
from django.shortcuts import render, redirect
from django.utils.timezone import now
from django.urls import reverse_lazy

from .models import Menu, Meal, Order, Employee
from .forms import CreateMenuForm, CreateMealForm, EmployeeOrderForm

decorators = [login_required]

from django.conf import settings
import uuid


@method_decorator(decorators, name='dispatch')
class CreateMenu(FormView):
    """
    Create a new menu, only menu no meals.
    """
    template_name = 'menus/menu_form.html'
    form_class = CreateMenuForm
    success_url = '/menu/'
    page_name = "New Menu"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


@method_decorator(decorators, name='dispatch')
class UpdateMenu(UpdateView):
    """
    Update a existing menu.
    """
    model = Menu
    fields = ['scheduled_to']
    template_name = 'menus/menu_form.html'
    success_url = '/menu/' # Menu list


@method_decorator(decorators, name='dispatch')
class ListMenu(ListView):
    """
    List of all menus created.
    """
    template_name = "menus/menus.html"
    model = Menu
    page_name = "Menus"
    context_object_name = "menus"


@method_decorator(decorators, name='dispatch')
class CreateMeal(FormView):
    """
    Create a new meal, just one.
    """
    template_name = 'meals/meal_form.html'
    form_class = CreateMealForm
    success_url = '/menu/'
    page_name = "New Meal"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


@method_decorator(decorators, name='dispatch')
class UpdateMeal(UpdateView):
    """
    Update a existing meal, just one.
    """
    model = Meal
    fields = ['name', 'menu']
    template_name = 'meals/meal_form.html'
    success_url = '/menu/' # Meal list. Not done


@method_decorator(decorators, name='dispatch')
class ListMeal(ListView):
    """
    List of all meals created.
    """
    template_name = "meals/meals.html"
    model = Meal
    page_name = "Meals"
    context_object_name = "meals"


# Create Order. No login required
class CreateOrder(CreateView):
    """
    No login required.
    View for employees to select they daily meal.
    If the employee isnt registered, it will register it.
    Function get_initial will set the values of all meals corresponding
     to the menu given(UUID).
    Function get will search for the menu for the given uuid.
    """
    model = Order
    template_name = 'orders/order_form.html'
    form_class = EmployeeOrderForm
    success_url = '/menu/orders/thanks/' # Should be outside of the app.
    # page_name = "Create order"

    def get_initial(self, **kwargs):
        initials = super(CreateOrder, self).get_initial()
        menu = Menu.objects.get(uuid=self.kwargs.get("uuid"))
        initials['menu'] = menu
        initials['meals'] = Meal.objects.filter(menu=menu)
        return initials

    # Request the createOrder page with GET method.
    def get(self, request, uuid, *args, **kwargs):
        form = self.form_class(initial=self.get_initial())
        menu = Menu.objects.get(uuid=self.kwargs.get("uuid"))
        return render(request, self.template_name, {'form':form, 'menu':menu})

    # Is form is valid do this.
    def form_valid(self, form):
        # employee is valid, set employee
        try:
            employee = Employee.objects.get(rut=form.cleaned_data['rut'])
        except Employee.DoesNotExist:
            employee = Employee.objects.create(rut=form.cleaned_data['rut'])
        menu = Menu.objects.get(uuid=self.kwargs.get("uuid"))
        form.instance.employee = employee
        form.instance.menu = menu
        return super(CreateOrder, self).form_valid(form)



class UpdateOrder(UpdateView):
    """
    Update order, no login required.
    """
    model = Order
    fields = ['meal', 'customization']
    template_name = 'orders/order_form.html'
    success_url = '/menu/orders/thanks/'  # Should be outside of the app


@method_decorator(decorators, name='dispatch')
class ListOrder(ListView):
    """
    List of orders, only available for Nora.
    """
    template_name = 'orders/orders.html'
    model = Order
    page_name = 'Orders list'
    context_object_name = 'orders'


def todays_menu(request):
    template = "menus/menu_detail.html"
    todays_date = now()
    try:
        menu = Menu.objects.get(scheduled_to=todays_date)
    except Menu.DoesNotExist:
        return redirect(reverse_lazy("first-page"))
    if menu:
        meals = Meal.objects.filter(menu=menu)
        data = {
            "menu":menu,
            "meals":meals
        }
        return render(request, template, data)
    else: 
        return redirect(reverse_lazy("first-page"))
        

